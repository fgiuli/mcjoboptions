evgenConfig.tune = "NNPDF3.1 LO"

genSeq.Sherpa_i.Parameters += [
    "PDF_LIBRARY=LHAPDFSherpa",
    "USE_PDF_ALPHAS=1",
    "PDF_SET=NNPDF31_lo_as_0118",
    "PDF_VARIATIONS=NNPDF31_lo_as_0118[all] NNPDF30_nnlo_as_0118 NNPDF31_nlo_as_0118",
    ]
