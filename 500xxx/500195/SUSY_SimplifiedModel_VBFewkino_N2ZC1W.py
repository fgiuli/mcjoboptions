#//////////////////////////////////////////////////////////////////////////////
# Standard pre-include
#--------------------------------------------------------------
include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py' )
keepOutput=False
#//////////////////////////////////////////////////////////////////////////////

#//////////////////////////////////////////////////////////////////////////////
# Some additional options to specify via preExec's.  Used during JO validation,
# not needed in production, but leave it in for future R&D.
# Nominal conditions:
#   -- Pythia parton shower
#   -- 2 parton emmission in matrix element
#   -- Don't use madgraph decays
#   -- Using madspin is ideal for correct m(ll) distributions
#--------------------------------------------------------------

# decays.  currently MG decays are not supported, so this flag doesn't do anything.
if 'madgraphdecays' not in dir(): madgraphdecays=False

# pythia option
if 'dipoleRecoil'   not in dir(): dipoleRecoil=1

# kinematics
if 'mmjj'           not in dir(): mmjj=500
if 'ptj'            not in dir(): ptj=20
if 'deltaeta'       not in dir(): deltaeta=3.0

# 4FNS vs 5FNS, read like "b in proton"
if 'binproton'      not in dir(): binproton=False

# extra QCD jets aside from VBF jets...  won't work with pure QED samples.
if 'extrajets'      not in dir(): extrajets=0

# fix Madgraph LHE files to include correct XS after MadSpin
if 'fixLHE'         not in dir(): fixLHE=True

# large value to keep intermediate off-shell bosons
if 'bwcut'          not in dir(): bwcut=10000

# merging options
if 'domerge'        not in dir(): domerge=False # overrides all merging options below this
if 'guessproc'      not in dir(): guessproc=False
if 'newxqcut'       not in dir(): newxqcut=30
#//////////////////////////////////////////////////////////////////////////////

#//////////////////////////////////////////////////////////////////////////////
# Interpret JobOption parameters
#--------------------------------------------------------------

def MassToFloat(s):
    if "p" in s:
        return float(s.replace("p", "."))
    return float(s)

# split up the JO file input name to interpret it
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
phys_short = get_physics_short()
splitConfig = get_physics_short().split('_')
joboptprocess = splitConfig[2]
# interpret the generation type, so we know which processes to run.
if not 'VBF' in joboptprocess:
    raise RuntimeError("This control file is only for EWKino VBF production")

photon=""
if joboptprocess[-1:]=="a":
    photon="a"

# figure out if this is Higgsino or Wino/Bino
higgsino=False
winobino=False
if "HH" in joboptprocess:
    higgsino=True
elif "WB" in joboptprocess:
    winobino=True
else:
    raise RuntimeError("Only Higgsino or WinoBino supported.")

# handle MadSpin configuration via JO name:
madspindecays=False
pythiadecays=True
if "MS" in phys_short:
    madspindecays=True
    pythiadecays=False

# set the decays in the param card.  these don't depend on wino vs bino.
decays['1000024']="""DECAY   1000024     7.00367294E-03   # chargino1+ decays 
     1.00000000E+00    2     1000022        24             # BR(~chi_1+ -> ~chi_10 w+)"""
decays['1000023']="""DECAY   1000023     9.37327589E-04   # neutralino2 decays
     1.00000000E+00    2     1000022        23             # BR(~chi_20 -> ~chi_10   Z )"""

# interpret the mass splittings, C1/N2 nearly degenerate
dM=MassToFloat(splitConfig[3])-MassToFloat(splitConfig[4])
print "Mass splitting = " , dM
masses['1000022'] =  1.0 * MassToFloat(splitConfig[4]) # LSP mass

# exact mass spectrum and mixing matrices depend on scenario
if higgsino:
    masses['1000023'] = -1.0 * MassToFloat(splitConfig[3]) # N2, make LSP higgsino like (positive for wino)
    masses['1000024'] =  0.5 * (MassToFloat(splitConfig[3])+MassToFloat(splitConfig[4])) # Chargino is between N1 and N2
    # Off-diagonal chargino mixing matrix V
    param_blocks['VMIX']={}
    param_blocks['VMIX']['1 1']='0.00E+00'
    param_blocks['VMIX']['1 2']='1.00E+00'
    param_blocks['VMIX']['2 1']='1.00E+00'
    param_blocks['VMIX']['2 2']='0.00E+00'
    # Off-diagonal chargino mixing matrix U
    param_blocks['UMIX']={}
    param_blocks['UMIX']['1 1']='0.00E+00'
    param_blocks['UMIX']['1 2']='1.00E+00'
    param_blocks['UMIX']['2 1']='1.00E+00'
    param_blocks['UMIX']['2 2']='0.00E+00'
    # Neutralino mixing matrix chi_i0 = N_ij (B,W,H_d,H_u)_j
    param_blocks['NMIX']={}
    param_blocks['NMIX']['1  1']=' 0.00E+00'   # N_11 bino 
    param_blocks['NMIX']['1  2']=' 0.00E+00'   # N_12
    param_blocks['NMIX']['1  3']=' 7.07E-01'   # N_13
    param_blocks['NMIX']['1  4']='-7.07E-01'   # N_14 
    param_blocks['NMIX']['2  1']=' 0.00E+00'   # N_21 
    param_blocks['NMIX']['2  2']=' 0.00E+00'   # N_22
    param_blocks['NMIX']['2  3']='-7.07E-01'   # N_23 higgsino
    param_blocks['NMIX']['2  4']='-7.07E-01'   # N_24 higgsino 
    param_blocks['NMIX']['3  1']=' 1.00E+00'   # N_31 
    param_blocks['NMIX']['3  2']=' 0.00E+00'   # N_32 
    param_blocks['NMIX']['3  3']=' 0.00E+00'   # N_33 higgsino
    param_blocks['NMIX']['3  4']=' 0.00E+00'   # N_34 higgsino
    param_blocks['NMIX']['4  1']=' 0.00E+00'   # N_41
    param_blocks['NMIX']['4  2']='-1.00E+00'   # N_42 wino
    param_blocks['NMIX']['4  3']=' 0.00E+00'   # N_43
    param_blocks['NMIX']['4  4']=' 0.00E+00'   # N_44
elif winobino:
    masses['1000023'] =  1.0 * MassToFloat(splitConfig[3]) # N2, make LSP wino like (negative for higgsino)
    masses['1000024'] =  masses['1000023'] # C1 = N2

    # mixing matrices were different, but now moved to the scheme used in the LHC SUSY page: https://twiki.cern.ch/twiki/pub/LHCPhysics/SUSYCrossSections13TeVn2x1wino/wino.dat
    # note that this zeroes out cross sections for e.g. N2N1 and N1C1 that were previously small but non-zero
    #
    # Chargino mixing matrix V
    param_blocks['VMIX']={}
    param_blocks['VMIX']['1  1']=' 1.00E+00'   # V_11
    param_blocks['VMIX']['1  2']=' 0.00E+00'   # V_12
    param_blocks['VMIX']['2  1']=' 0.00E+00'   # V_21
    param_blocks['VMIX']['2  2']=' 0.00E+00'   # V_22
    # Chargino mixing matrix U
    param_blocks['UMIX']={}
    param_blocks['UMIX']['1  1']=' 1.00E+00'   # U_11
    param_blocks['UMIX']['1  2']=' 0.00E+00'   # U_12
    param_blocks['UMIX']['2  1']=' 0.00E+00'   # U_21
    param_blocks['UMIX']['2  2']=' 0.00E+00'   # U_22
    # Neutralino mixing matrix
    param_blocks['NMIX']={}
    param_blocks['NMIX']['1  1']=' 1.00E+00'   # N_11 bino
    param_blocks['NMIX']['1  2']=' 0.00E+00'   # N_12
    param_blocks['NMIX']['1  3']=' 0.00E+00'   # N_13
    param_blocks['NMIX']['1  4']=' 0.00E+00'   # N_14
    param_blocks['NMIX']['2  1']=' 0.00E+00'   # N_21
    param_blocks['NMIX']['2  2']=' 1.00E+00'   # N_22 wino
    param_blocks['NMIX']['2  3']=' 0.00E+00'   # N_23 
    param_blocks['NMIX']['2  4']=' 0.00E+00'   # N_24 
    param_blocks['NMIX']['3  1']=' 0.00E+00'   # N_31
    param_blocks['NMIX']['3  2']=' 0.00E+00'   # N_32
    param_blocks['NMIX']['3  3']=' 0.00E+00'   # N_33 higgsino
    param_blocks['NMIX']['3  4']=' 0.00E+00'   # N_34 higgsino
    param_blocks['NMIX']['4  1']=' 0.00E+00'   # N_41
    param_blocks['NMIX']['4  2']=' 0.00E+00'   # N_42
    param_blocks['NMIX']['4  3']=' 0.00E+00'   # N_43 higgsino
    param_blocks['NMIX']['4  4']=' 0.00E+00'   # N_44 higgsino

#//////////////////////////////////////////////////////////////////////////////


#//////////////////////////////////////////////////////////////////////////////
# MadGraph5 Options
#--------------------------------------------------------------
run_settings['ptl']=0
run_settings['pta']=10
run_settings['etal']='-1.0'
run_settings['drll']=0.0
run_settings['drjl']=0.0
run_settings['lhe_version']='3.0'
run_settings['auto_ptj_mjj']='F'
run_settings['bwcutoff']=bwcut # Set to something big like 10000 to allow very low-mass W* and Z*
run_settings['event_norm']='sum' # this gets overridden in MGC for 21.6.12+, leaving in here for older releases.
run_settings['use_syst']='F'

## Andy's config from ATLAS-SUSY-2018-16
#run_settings['ptj']=12
#run_settings['mmjj']=200 # Enrich with target process
##run_settings['deltaeta']=3.0
#newxqcut = 15             # low matching scale, following DM group recommendations

## CMS config from https://arxiv.org/abs/1905.13059
#run_settings['ptj']=30
#run_settings['deltaeta']=3.5
#newxqcut = 40             # matching scale should be above ptj

# Compromise
run_settings['ptj']=ptj
run_settings['mmjj']=mmjj
run_settings['deltaeta']=deltaeta

if domerge:
    xqcut=newxqcut
    # will be two, unless otherwise specified.
    njets=extrajets # for 'c1 n2 j j j', should this be njets=1, or njets=3?
else:
    print "Not doing merging: njets=0 and xqcut=-1"
    xqcut=-1
    njets=0

includeb=""
if binproton:
    includeb="b b~"

process='''
define p = g u c d s u~ c~ d~ s~ %s
define f = e+ mu+ ta+ e- mu- ta- ve vm vt ve~ vm~ vt~ u u~ d d~ c c~ s s~ b b~ g
''' % includeb

if "QQ" in joboptprocess:
    # remove gluons from proton and jets if we're running "q q > ino ino q q"
    process+='''
define p =   u c d s u~ c~ d~ s~ %s
define j =   u c d s u~ c~ d~ s~ %s
''' % (includeb,includeb)
#//////////////////////////////////////////////////////////////////////////////


#//////////////////////////////////////////////////////////////////////////////
# Configure the process definitions.
#--------------------------------------------------------------
mgprocstring=""
if 'C1C1' in joboptprocess or 'CpCm' in joboptprocess or 'CmCp' in joboptprocess:
    msdecaystring="decay x1+ > f f n1 \ndecay x1- > f f n1\n" # C1C1 decay to f f gives us our target fermionic final state
    mgprocstring="x1+ x1-"
    hardproc="pp>{~chi_1+,1000024}{~chi_1-,-1000024}"
elif 'CpCp' in joboptprocess:
    msdecaystring="decay x1+ > f f n1\ndecay x1+ > f f n1\n" # C1C1 decay to f f gives us our target fermionic final state
    mgprocstring="x1+ x1+"
    hardproc="pp>{~chi_1+,1000024}{~chi_1+,1000024}"
elif 'CmCm' in joboptprocess:
    msdecaystring="decay x1- > f f n1\ndecay x1- > f f n1\n" # C1C1 decay to f f gives us our target fermionic final state
    mgprocstring="x1- x1-"
    hardproc="pp>{~chi_1-,-1000024}{~chi_1-,-1000024}"
elif 'N2N1' in joboptprocess or 'N1N2' in joboptprocess:
    msdecaystring="decay n2 > f f  n1\n" # N2 to fermions
    mgprocstring="n2 n1"
    hardproc="pp>{~chi_20,1000023}{~chi_10,1000022}"
elif 'N2C1m' in joboptprocess or 'C1mN2' in joboptprocess:
    msdecaystring="decay n2 > f f n1\ndecay x1- > f f n1\n" # N2 to fermions, C1 to hadrons
    mgprocstring="n2 x1-"
    hardproc="pp>{~chi_1-,-1000024}{~chi_20,1000023}"
elif 'N2C1p' in joboptprocess or 'C1pN2' in joboptprocess:
    msdecaystring="decay n2 > f f n1\ndecay x1+ > f f n1\n" # N2 to fermions, C1 to hadrons
    mgprocstring="n2 x1+"
    hardproc="pp>{~chi_1+,1000024}{~chi_20,1000023}"
elif 'N1C1m' in joboptprocess or 'C1mN2' in joboptprocess:
    msdecaystring="decay x1- > f f n1\n" # C1 to fermions
    mgprocstring="n1 x1-"
    hardproc="pp>{~chi_1-,-1000024}{~chi_10,1000022}"
elif 'N1C1p' in joboptprocess or 'C1pN2' in joboptprocess:
    msdecaystring="decay x1+ > f f n1\n" # C1 to fermions
    mgprocstring="n1 x1+"
    hardproc="pp>{~chi_1+,1000024}{~chi_10,1000022}"
else:
    raise RuntimeError("Unknown process %s, aborting." % joboptprocess)

mgprocstring="p p > %s %s j j" % (mgprocstring,photon)

if 'QED' in joboptprocess:
    # Want to isolate the QED contribution, turn off QCD.  Don't consider any extra jets in this scenario.
    process += "%-12s %s / susystrong QED=99 QCD=0 @%d\n" % ('generate', mgprocstring, 1)
else:
    # Inclusive process, by default.  Can include extra jets, beyond the 2 VBF jets.
    process += "%-12s %s / susystrong QED=99 QCD=99 @%d\n" % ('generate', mgprocstring, 1)
    # extra jets, if we want them.
    # (we probably don't want them for VBF, and recent versions of Pythia crash on merging:
    # https://its.cern.ch/jira/browse/AGENE-1783, so don't do this for now.)
    for i in xrange(extrajets):
        process += "%-12s %s %s / susystrong QED=99 QCD=99 @%d\n" % ('add process', mgprocstring, " j"*(i+1), 1)

# print the process, just to confirm we got everything right
print "Final process card:"
print process

#//////////////////////////////////////////////////////////////////////////////


#//////////////////////////////////////////////////////////////////////////////
# Madspin configuration
#--------------------------------------------------------------
if madspindecays==True and ((not hasattr(runArgs, "inputGeneratorFile")) or runArgs.inputGeneratorFile == ""):
    fixEventWeightsForBridgeMode=fixLHE
    if msdecaystring=="":
        raise RuntimeError("Asking for MadSpin decays, but no decay string provided!")
    madspin_card='madspin_card_vbf_susy.dat'
    mscard = open(madspin_card,'w')

    mscard.write("""
#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************
#Some options (uncomment to apply)
#
# set Nevents_for_max_weigth 75 # number of events for the estimate of the max. weight
set BW_cut 10000                # cut on how far the particle can be off-shell
set max_weight_ps_point 400  # number of PS to estimate the maximum for each event
#
set seed %i
set spinmode none
# specify the decay for the final state particles

%s

# running the actual code
launch""" % (runArgs.randomSeed,msdecaystring))

    mscard.close()
#
#//////////////////////////////////////////////////////////////////////////////


#//////////////////////////////////////////////////////////////////////////////
# Pythia options
#--------------------------------------------------------------
pythia = genSeq.Pythia8
pythia.Commands += ["23:mMin = 0.2"]
pythia.Commands += ["24:mMin = 0.2"]
pythia.Commands += ["-24:mMin = 0.2"]
if dipoleRecoil==1:
    pythia.Commands += ["SpaceShower:dipoleRecoil=on"] # ATLAS-PHYS-PUB-2019-004 recommends "on" for VBF/VBS.


# information about this generation
evgenLog.info('Registered generation of EWKino-like ~chi2/~chi1/~chi1+/~chi1- production via VBF, decay via SM W/Z to fermions; grid point decoded into LSP mass point ' +str(masses['1000022'])+ ' with ' +str(dM)+ ' GeV splitting')
evgenConfig.contact  = [ "giordon.holtsberg.stark@cern.ch", "michael.hance@cern.ch" ]
evgenConfig.keywords += ['SUSY', 'VBF', 'MSSM', 'chargino', 'neutralino']
evgenConfig.description = 'SUSY Simplified Model with compressed EWKinos with VBF production and decays via a SM W/Z to fermions'
#//////////////////////////////////////////////////////////////////////////////


#//////////////////////////////////////////////////////////////////////////////
# Adding some filters - MET and Lepton Filters
# include check on dM in case we just want to ask MadGraph to generate completely degenerate Higgsinos,
# e.g. to check cross sections.
#--------------------------------------------------------------
evt_multiplier = 2 # seems to be sufficient for MET>75 filter
if domerge:
    evt_multiplier*= 2 # for CKKW-L merging
filters=[]

# lepton filter, interpreted from jobOptions
nleptonsfilterreq=0
if   '1L' in phys_short:
    nleptonsfilterreq=1
elif '2L' in phys_short:
    nleptonsfilterreq=2

if (nleptonsfilterreq > 0) and (dM > 0):
    if   dM>=20:
        evt_multiplier *= 2
    if   dM>=10:
        evt_multiplier *= 3
    elif dM>=5:
        evt_multiplier *= 4
    elif dM>=4:
        evt_multiplier *= 5
    elif dM>=3:
        evt_multiplier *= 8
    elif dM>=2:
        evt_multiplier *= 10
    elif dM>=1:
        evt_multiplier *= 15
    else:
        raise RuntimeError("Couldn't properly set evt_multiplier (mass splitting below 1 GeV not currently allowed), exiting")


    from GeneratorFilters.GeneratorFiltersConf import MultiElecMuTauFilter

    filtSeq += MultiElecMuTauFilter("MultiElecMuTauFilter")
    ElecMuTauFilter = filtSeq.MultiElecMuTauFilter
    ElecMuTauFilter.MinPt = 2000.
    ElecMuTauFilter.MaxEta = 2.8
    ElecMuTauFilter.NLeptons = nleptonsfilterreq
    ElecMuTauFilter.IncludeHadTaus = 0    # don't include hadronic taus

    filters.append("MultiElecMuTauFilter")

# MET filter, interpreted from jobOptions
if 'MET' in phys_short and dM > 0:
    if not hasattr(filtSeq, "MissingEtFilter"):
        from GeneratorFilters.GeneratorFiltersConf import MissingEtFilter
        filtSeq += MissingEtFilter("MissingEtFilter")

    if not hasattr(filtSeq, "MissingEtFilterUpperCut"):
        filtSeq += MissingEtFilter("MissingEtFilterUpperCut")

    lowercut_start = phys_short.find('MET')+3
    lowercut_end = phys_short.find('_',phys_short.find('MET')+1)
    if lowercut_end<0: lowercut_end=len(phys_short)
    lowercut = int(phys_short[lowercut_start:lowercut_end])

    filtSeq.MissingEtFilter.METCut          = lowercut*GeV
    filtSeq.MissingEtFilterUpperCut.METCut  = 100000*GeV
    filters.append("MissingEtFilter and not MissingEtFilterUpperCut")

# only add these filters if we have some non-zero mass splitting.  dM=0 is a special case used to test
# output cross sections against standard benchmarks
if dM > 0 and len(filters):
    filtSeq.Expression='('+( ') and ('.join(filters) )+')'

evgenLog.info("evt_multiplier == " + str(evt_multiplier))
#//////////////////////////////////////////////////////////////////////////////

#//////////////////////////////////////////////////////////////////////////////
# Standard post-include
#--------------------------------------------------------------
include('MadGraphControl/SUSY_SimplifiedModel_PostInclude.py')
#//////////////////////////////////////////////////////////////////////////////

#//////////////////////////////////////////////////////////////////////////////
# Merging options
#--------------------------------------------------------------
if domerge:
    mergeproc="Merging:Process = %s" % hardproc
    if guessproc:
        mergeproc="Merging:Process = guess"
    if njets > 0:
        genSeq.Pythia8.Commands += [ "%s" % mergeproc,
                                     "1000024:spinType = 1",
                                     "1000023:spinType = 1",
                                     "1000022:spinType = 1" ]

        if "guess" in mergeproc:
            genSeq.Pythia8.UserHooks += ['JetMergingaMCatNLO']
#//////////////////////////////////////////////////////////////////////////////
