include("Sherpa_i/2.2.8_NNPDF30NNLO.py")
include("Sherpa_i/2.2.8_OpenLoopsCvmfs_Plugin.py")

evgenConfig.description = "Sherpa llgamma + 0,1j@NLO + 2,3j@LO with 7<pT_y"
evgenConfig.keywords = ["SM", "2muon", "photon", "NLO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "schae@cern.ch" ]
evgenConfig.nEventsPerJob = 200

genSeq.Sherpa_i.RunCard="""
(run){
  % Reduction in negative weights
  NLO_CSS_PSMODE=1

  # HT prime scale 
  PP_RS_SCALE VAR{sqr(sqrt(H_T2)-PPerp(p[2])-PPerp(p[3])+MPerp(p[2]+p[3]))/4};

  % scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  % NLO corrections EWK
  OL_PARAMETERS=ew_scheme 2 ew_renorm_scheme 1
  ASSOCIATED_CONTRIBUTIONS_VARIATIONS=EW EW|LO1
  EW_SCHEME=3
  GF=1.166397e-5 
  METS_BBAR_MODE=5

  % tags for process setup
  NJET:=3; LJET:=3,4; QCUT:=20;

  % me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoopsCvmfs;
}(run)

(processes){
  Process 93 93 -> 22 13 -13 93{NJET}
  Enhance_Observable VAR{log10(max(PPerp(p[2]+p[3]),PPerp(p[4])))}|1|2.7 {3,4,5,6} 
  Associated_Contributions EW|LO1 {LJET};
  Order (*,3); CKKW sqr(QCUT/E_CMS)
  NLO_QCD_Mode MC@NLO {LJET}
  ME_Generator Amegic {LJET}
  RS_ME_Generator Comix {LJET}
  Loop_Generator LOOPGEN {LJET}
  PSI_ItMin 20000 {4}
  Integration_Error 0.99 {4}
  PSI_ItMin 50000 {5,6}
  Integration_Error 0.99 {5,6}
  End process

}(processes)

(selector){
  PTNLO  22  7  E_CMS
  IsolationCut  22  0.1  2  0.10
  DeltaRNLO  22  90  0.1 1000.0
  Mass  90  -90  10.0  E_CMS
}(selector)
"""

genSeq.Sherpa_i.NCores = 24

## taking OpenLoops from LCG cvmfs
#genSeq.Sherpa_i.OpenLoopsLibs = [ "pplla", "ppllaj","pplla_ew","ppllaj_ew"]
